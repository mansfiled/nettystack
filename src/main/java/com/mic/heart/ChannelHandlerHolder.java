package com.mic.heart;

import io.netty.channel.ChannelHandler;

/**
 * Created by hy06 on 2018/2/12.
 */
public interface ChannelHandlerHolder {
    ChannelHandler[] handlers();
}
