package com.mic.thread;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

/**
 * Created by hy06 on 2018/2/14.
 */
public class SimpleWn {

    final static List<?> list = new ArrayList<>();

    private static class T1 extends Thread {
        @Override
        public void run() {
            synchronized (list) {
                out.println("t1 start wait");
                try {
                    list.wait();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                out.print("t1 wait end");
            }
        }
    }
    private static class T2 extends Thread {
        @Override
        public void run() {
            synchronized (list) {
                out.println("t2 start notify");
                list.notify();
                out.println("t2 notify end");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new T1();
        Thread t2 = new T2();
        t1.start();
        t2.start();
        t1.join();
        t2.join();
    }
}
