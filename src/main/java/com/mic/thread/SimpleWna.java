package com.mic.thread;

import static java.lang.System.out;

/**
 * Created by hy06 on 2018/2/14.
 */
public class SimpleWna {
    final static Object object = new Object();

    public static class T1 extends Thread {
        @Override
        public void run() {
            synchronized (object) {
                out.println("t1 start! wait on object");
                try {
                    object.wait();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                out.println("t1 end !");
            }
        }
    }
    public static class T2 extends Thread {
        @Override
        public void run() {
            synchronized (object) {
                out.println("t2 start notify all threads");
                object.notifyAll();
                out.println("t2 end !");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new T1();
        Thread t1_1 = new T2();
        t1.start();
        t1_1.start();
        Thread.sleep(1000);
        Thread t2 = new T2();
        t2.start();
    }
}
