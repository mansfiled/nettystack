package com.mic.current.collect;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by yjq14 on 2018/2/21.
 */
public class CollentDemo {
    public static void main(String[] args) {
        List<Object> list = Collections.synchronizedList(new ArrayList<>());
        Map<Object, Object> map = Collections.synchronizedMap(new HashMap<>());

        ConcurrentHashMap<String, Object> concurrentHashMap = new ConcurrentHashMap<>();
//        BlockingQueue<String>
        ConcurrentLinkedQueue<String> queue = new ConcurrentLinkedQueue<>();

    }
}
