package com.mic.current.pool.simple;

import java.util.List;
import java.util.Vector;

/**
 * Created by hy06 on 2018/2/22.
 */
public class ThreadPool {
    private static ThreadPool instance = null;

    // 空闲的线程队列

    private List<Worker> idleThreads;
    // 已有的线程总数

    private int threadCounter;

    private boolean isShutDown = false;

    private ThreadPool() {
        this.idleThreads = new Vector(5);
        threadCounter = 0;
    }
    // 将线程放入池中

    protected synchronized void repool(Worker repoolingThread){
       if (!isShutDown) {
           idleThreads.add(repoolingThread);
       } else {
           // 关闭线程
       }
    }

    protected synchronized void shutDown() {
        isShutDown = true;
        for (int threadIndex = 0; threadIndex < idleThreads.size(); threadIndex++) {
            Worker idleThread = idleThreads.get(threadIndex);

        }
    }
}
