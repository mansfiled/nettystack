package com.mic.current;

import java.util.concurrent.locks.LockSupport;

/**
 * Created by yjq14 on 2018/2/21.
 */
public class LockSupportDemo {
    public static Object u = new Object();
    static ChangeObjectTread t1 = new ChangeObjectTread("t1");
    static ChangeObjectTread t2 = new ChangeObjectTread("t2");
    public static class ChangeObjectTread extends Thread {
        public ChangeObjectTread(String name) {
            super.setName(name);
        }

        @Override
        public void run() {
            synchronized (u) {
                System.out.println("in" +getName());
                LockSupport.park();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        t1.start();
        Thread.sleep(100);
        t2.start();
        LockSupport.unpark(t1);
        LockSupport.unpark(t2);
        t1.join();
        t2.join();
    }
}
