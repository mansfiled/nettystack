package com.mic.current;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 重入锁，一个线程 一个许可
 *
 * Created by yjq14 on 2018/2/19.
 */
public class ReenterLock implements Runnable{
    private static ReentrantLock lock = new ReentrantLock();
    public static int k = 0;
    @Override
    public void run() {
        for (int i = 0; i < 10000000; i++) {
            lock.lock();
            try{
                k++;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock(); // 不能忘记释放锁
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new ReenterLock());
        Thread t2 = new Thread(new ReenterLock());
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println(k);
    }
}
