package com.mic.atom;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by yjq14 on 2018/2/19.
 */
public class AtomicIntegerDemo {
    static AtomicInteger i = new AtomicInteger();
    static Integer a = 1;
    public static class AddThread implements Runnable{

        @Override
        public void run() {
            for (int j = 0; j < 10000; j++) {
//                i.incrementAndGet();
                a++;
            }
        }

        public static void main(String[] args) throws InterruptedException {
            Thread[] ts = new Thread[10];
            for (int j = 0; j < 10; j++) {
                ts[j] = new Thread(new AddThread());
            }
            for (int j = 0; j < 10; j++) {
                ts[j].start();
            }
            for (int j = 0; j < 10; j++) {
                ts[j].join();
            }
            System.out.println(i);
        }
    }
}
