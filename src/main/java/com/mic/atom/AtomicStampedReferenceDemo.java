package com.mic.atom;

import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * Created by yjq14 on 2018/2/19.
 */
public class AtomicStampedReferenceDemo {
    static AtomicStampedReference<Integer> money = new AtomicStampedReference<>(19,0);

    public static void main(String[] args) {
        // 模拟多个线程同时更新后台数据库，为用户充值
        for (int i = 0; i < 3; i++) {
            final int timeStamp = money.getStamp();
            new Thread(() -> {
                while (true) {
                    while (true) {
                        Integer m = money.getReference();
                        if (m < 20) {
                            if (money.compareAndSet(m, m + 20, timeStamp, timeStamp + 1)) {
                                System.out.println("余额小于20块，充值成功，余额：" + money.getReference() + "元");
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
            }).start();
        }
        // 用户消费线程，模拟消费行为

        new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                while (true) {
                    int stamp = money.getStamp();
                    Integer m = money.getReference();
                    if (m > 10) {
                        System.out.println("大于10元");
                        if (money.compareAndSet(m, m-10, stamp, stamp + 1)){
                            System.out.println("成功消费10元, 余额" + money.getReference());
                            break;
                        }
                    } else {
                        System.out.println("没有足够的金额");
                        break;
                    }

                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
