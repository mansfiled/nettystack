package com.mic.atom;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by yjq14 on 2018/2/19.
 */
public class AtomicReferenceTest {
    private final static AtomicReference<String> atomicStr = new AtomicReference<>("abc");

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                try {
                    Thread.sleep((int) Math.random() * 100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (atomicStr.compareAndSet("abc", "def"))
                    System.out.println("Thread" + Thread.currentThread().getId() + "change vaule to def ");
                else
                    System.out.println("thread" + Thread.currentThread().getId() + "change failed");
            }).start();

        }} }
