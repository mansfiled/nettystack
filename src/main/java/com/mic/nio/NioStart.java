package com.mic.nio;

import com.mic.nio.pool.NioSelectorRunnablePool;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import static java.lang.System.out;

/**
 * Created by yjq14 on 2018/2/10.
 */
public class NioStart {
    /**
     * @param args
     */
    public static void main(String[] args) {

        NioSelectorRunnablePool selectorRunnablePool = new NioSelectorRunnablePool(Executors.newCachedThreadPool(), Executors.newCachedThreadPool());

        ServerBootstrap serverBootstrap = new ServerBootstrap(selectorRunnablePool);

        serverBootstrap.bind(new InetSocketAddress(1001));

        out.println("start");
    }
}
