package com.mic.nio;

import com.mic.nio.pool.Boss;
import com.mic.nio.pool.NioSelectorRunnablePool;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.channels.ServerSocketChannel;

/**
 * Created by yjq14 on 2018/2/10.
 */
public class ServerBootstrap {
    private NioSelectorRunnablePool nioSelectorRunnablePool;

    public ServerBootstrap(NioSelectorRunnablePool nioSelectorRunnablePool) {
        this.nioSelectorRunnablePool = nioSelectorRunnablePool;
    }

    public void bind(final SocketAddress localAddress) {
        try {
            ServerSocketChannel socketChannel = ServerSocketChannel.open();
            socketChannel.configureBlocking(false);
            socketChannel.socket().bind(localAddress);
            Boss boss = nioSelectorRunnablePool.nextBoss();
            boss.registerAcceptChannelTask(socketChannel);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
