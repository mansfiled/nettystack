package com.mic.nio.pool;

import java.nio.channels.ServerSocketChannel;

/**
 * boss 接口
 * Created by yjq14 on 2018/2/10.
 */
public interface Boss {
    void registerAcceptChannelTask(ServerSocketChannel serverChannel);
}
