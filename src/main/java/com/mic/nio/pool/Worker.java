package com.mic.nio.pool;

import java.nio.channels.SocketChannel;

/**
 * Created by yjq14 on 2018/2/10.
 */
public interface Worker {
    void registerNewChannelTask(SocketChannel channel);
}
