package com.mic.netty.marshaller;

import io.netty.handler.codec.marshalling.DefaultMarshallerProvider;
import io.netty.handler.codec.marshalling.DefaultUnmarshallerProvider;
import io.netty.handler.codec.marshalling.MarshallingDecoder;
import io.netty.handler.codec.marshalling.MarshallingEncoder;
import org.jboss.marshalling.*;

import java.io.IOException;

/**
 * @author yangjiaqi
 * @create 2018-02-02 13:50
 */
public final class MarshallingCodeCFactory {

    public static MarshallingDecoder buildMarshallingDecoder(){
        final MarshallerFactory serial = Marshalling.getProvidedMarshallerFactory("serial");
        final MarshallingConfiguration configuration = new MarshallingConfiguration();
        configuration.setVersion(5);
        DefaultUnmarshallerProvider provider = new DefaultUnmarshallerProvider(serial, configuration);
        return new MarshallingDecoder(provider, 1024);
    }
    public static MarshallingEncoder buildMarshallingEncoder(){
        final MarshallerFactory serial = Marshalling.getProvidedMarshallerFactory("serial");
        final MarshallingConfiguration configuration = new MarshallingConfiguration();
        configuration.setVersion(5);
        DefaultMarshallerProvider provider = new DefaultMarshallerProvider(serial, configuration);
        return new MarshallingEncoder(provider);
    }
    public static Marshaller buildMarshalling() throws IOException {
        final MarshallerFactory serial = Marshalling.getProvidedMarshallerFactory("serial");
        final MarshallingConfiguration configuration = new MarshallingConfiguration();
        configuration.setVersion(5);
        return serial.createMarshaller(configuration);
    }
    public static Unmarshaller buildUnmarshalling() throws IOException {
        final MarshallerFactory marshallerFactory = Marshalling.getProvidedMarshallerFactory("serial");
        final MarshallingConfiguration configuration = new MarshallingConfiguration();
        configuration.setVersion(5);
        return marshallerFactory.createUnmarshaller(configuration);
    }

    public static void main(String[] args) throws IOException {
        Unmarshaller unmarshaller = MarshallingCodeCFactory.buildUnmarshalling();
        Marshaller marshaller = MarshallingCodeCFactory.buildMarshalling();
        System.out.println(unmarshaller);
        System.out.println(marshaller);
    }
}
