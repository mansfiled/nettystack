package com.mic.netty.server;

import com.mic.netty.NettyConstant;
import com.mic.netty.codec.NettyMessageDecoder;
import com.mic.netty.codec.NettyMessageEncoder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.ReadTimeoutHandler;

public  class NettyServer {
    public void build() throws Exception {
        EventLoopGroup boosGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        ServerBootstrap b = new ServerBootstrap();
        b.group(boosGroup, workerGroup).channel(NioServerSocketChannel.class)
                .option(ChannelOption.SO_BACKLOG, 100)
                .handler(new LoggingHandler(LogLevel.INFO))
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    public void initChannel(SocketChannel s) throws Exception {
                        s.pipeline().addLast(new NettyMessageDecoder(1024 * 1024, 4, 4));
                        s.pipeline().addLast(new NettyMessageEncoder());
                        s.pipeline().addLast("readTimeoutHandler", new ReadTimeoutHandler(50));
                        s.pipeline().addLast(new LoginAuthRespHandler());
                        s.pipeline().addLast("heartBeatRespHandler", new HeartBeatRespHandler());
                    }
                });
        b.bind(NettyConstant.REMOTEIP, NettyConstant.PORT).sync();
        System.out.println("netty server start ok : " + (NettyConstant.REMOTEIP + ":" + NettyConstant.PORT));
    }

    public static void main(String[] args) throws Exception {
        new NettyServer().build();
    }
}
