package com.mic;

/**
 * Created by yjq14 on 2018/2/18.
 */
public class NoVisibillity {
    private static  boolean ready;

    private static  int number;

    private static class ReaderThread extends Thread {
        @Override
        public void run() {
            while (!ready) {
               Thread.yield();
            }
            System.out.println(number);
        }
    }

    public static void main(String[] args) {
        new ReaderThread().start();
        number = 42;
        ready = true;
    }
}
